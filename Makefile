.PHONY: clean all
all:
	dune build @install @runtest

clean:
	dune clean
